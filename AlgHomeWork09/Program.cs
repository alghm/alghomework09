﻿using System.Diagnostics;

internal class Program
{
    private static void Main(string[] args)
    {
        var array_1_100 = new UInt16[100];
        var array_2_100 = new UInt16[100];
        var array_3_100 = new UInt16[100];
        var array_1_1000 = new UInt16[1000];
        var array_2_1000 = new UInt16[1000];
        var array_3_1000 = new UInt16[1000];
        var array_1_10_000 = new UInt16[10_000];
        var array_2_10_000 = new UInt16[10_000];
        var array_3_10_000 = new UInt16[10_000];
        var array_1_100_000 = new UInt16[100_000];
        var array_2_100_000 = new UInt16[100_000];
        var array_3_100_000 = new UInt16[100_000];
        var array_1_1000_000 = new UInt16[1000_000];
        var array_2_1000_000 = new UInt16[1000_000];
        var array_3_1000_000 = new UInt16[1000_000];

        var array_1_10_000_000 = new UInt16[10_000_000];
        var array_2_10_000_000 = new UInt16[10_000_000];
        var array_3_10_000_000 = new UInt16[10_000_000];

        var array_2_100_000_000 = new UInt16[100_000_000];
        var array_3_100_000_000 = new UInt16[100_000_000];

        var array_2_1_000_000_000 = new UInt16[1_000_000_000];
        var array_3_1_000_000_000 = new UInt16[1_000_000_000];

        for (int i = 0; i < 100; i++)
        {
            var number = (UInt16)new Random().Next(0, 1000);
            array_1_100[i] = number;
            array_2_100[i] = number;
            array_3_100[i] = number;
        }

        for (int i = 0; i < 1000; i++)
        {
            var number = (UInt16)new Random().Next(0, 1000);
            array_1_1000[i] = number;
            array_2_1000[i] = number;
            array_3_1000[i] = number;
        }


        for (int i = 0; i < 10_000; i++)
        {
            var number = (UInt16)new Random().Next(0, 1000);
            array_1_10_000[i] = number;
            array_2_10_000[i] = number;
            array_3_10_000[i] = number;
        }

        for (int i = 0; i < 100_000; i++)
        {
            var number = (UInt16)new Random().Next(0, 1000);
            array_1_100_000[i] = number;
            array_2_100_000[i] = number;
            array_3_100_000[i] = number;
        }

        for (int i = 0; i < 1000_000; i++)
        {
            var number = (UInt16)new Random().Next(0, 1000);
            array_1_1000_000[i] = number;
            array_2_1000_000[i] = number;
            array_3_1000_000[i] = number;
        }


        for (int i = 0; i < 10_000_000; i++)
        {
            var number = (UInt16)new Random().Next(0, 65535);
            array_1_10_000_000[i] = number;
            array_2_10_000_000[i] = number;
            array_3_10_000_000[i] = number;
        }

        for (int i = 0; i < 100_000_000; i++)
        {
            var number = (UInt16)new Random().Next(0, 65535);
            array_2_100_000_000[i] = number;
            array_3_100_000_000[i] = number;
        }

        for (int i = 0; i < 1_000_000_000; i++)
        {
            var number = (UInt16)new Random().Next(0, 65535);
            array_2_1_000_000_000[i] = number;
            array_3_1_000_000_000[i] = number;
        }

        Stopwatch stopwatch = new Stopwatch();

        stopwatch.Start();
        BucketSort(array_1_100);
        stopwatch.Stop();
        long elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("BucketSort для 100 - {0}", elapsedTime);

        stopwatch.Restart();
        BucketSort(array_1_1000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("BucketSort для 1000 - {0}", elapsedTime);

        stopwatch.Restart();
        BucketSort(array_1_10_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("BucketSort для 10 000 - {0}", elapsedTime);

        stopwatch.Restart();
        BucketSort(array_1_100_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("BucketSort для 100 000 - {0}", elapsedTime);

        stopwatch.Restart();
        BucketSort(array_1_1000_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("BucketSort для 1 000 000 - {0}", elapsedTime);

        stopwatch.Restart();
        BucketSort(array_1_10_000_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("BucketSort для 10 000 000 - {0}", elapsedTime);

        stopwatch.Restart();
        CountingSort(array_2_100);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("CountingSort для 100 - {0}", elapsedTime);

        stopwatch.Restart();
        CountingSort(array_2_1000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("CountingSort для 1000 - {0}", elapsedTime);

        stopwatch.Restart();
        CountingSort(array_2_10_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("CountingSort для 10 000 - {0}", elapsedTime);

        stopwatch.Restart();
        CountingSort(array_2_100_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("CountingSort для 100 000 - {0}", elapsedTime);

        stopwatch.Restart();
        CountingSort(array_2_1000_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("CountingSort для 1 000 000 - {0}", elapsedTime);

        stopwatch.Restart();
        CountingSort(array_2_10_000_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("CountingSort для 10 000 000 - {0}", elapsedTime);

        stopwatch.Restart();
        CountingSort(array_2_100_000_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("CountingSort для 100 000 000 - {0}", elapsedTime);

        stopwatch.Restart();
        CountingSort(array_2_1_000_000_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("CountingSort для 1 000 000 000 - {0}", elapsedTime);

        stopwatch.Restart();
        RadixSort(array_3_100);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("RadixSort для 100 - {0}", elapsedTime);

        stopwatch.Restart();
        RadixSort(array_3_1000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("RadixSort для 1000 - {0}", elapsedTime);

        stopwatch.Restart();
        RadixSort(array_3_10_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("RadixSort для 10 000 - {0}", elapsedTime);

        stopwatch.Restart();
        RadixSort(array_3_100_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("RadixSort для 100 000 - {0}", elapsedTime);

        stopwatch.Restart();
        RadixSort(array_3_1000_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("RadixSort для 1 000 000 - {0}", elapsedTime);

        stopwatch.Restart();
        RadixSort(array_3_10_000_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("RadixSort для 10 000 000 - {0}", elapsedTime);
        
        stopwatch.Restart();
        RadixSort(array_3_100_000_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("RadixSort для 100 000 000 - {0}", elapsedTime);

        stopwatch.Restart();
        RadixSort(array_3_1_000_000_000);
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("RadixSort для 1 000 000 000 - {0}", elapsedTime);


        Console.ReadKey();
    }

    static void BucketSort(UInt16[] array)
    {
        int N = array.Length;

        int maxValue = array[0];

        foreach (var el in array)
        {
            if (el > maxValue)
            {
                maxValue = el;
            }
        }

        long arrayLength = (long) maxValue * N / (maxValue + 1) + 1;
        LinkedList<int>[] bucket = new LinkedList<int>[arrayLength];
        for (long i = 0; i < N; i++)
        {
            long nr = (long) array[i] * N / (maxValue + 1);
            if (bucket[nr] == null)
            {
                bucket[nr] = new LinkedList<int>();
            }
            bucket[nr].AddFirst(array[i]);
            var item = bucket[nr].First;

            while (item.Next != null)
            {
                if (item.Value <= item.Next.Value)
                {
                    break;
                }
                int x = item.Value;
                item.Value = item.Next.Value;
                item.Next.Value = x;
            };
        }

        var index = 0;

        for (int i = 0; i < bucket.Length; i++)
        {
            if (bucket[i] != null)
            {
                LinkedListNode<int> node = bucket[i].First;

                while (node != null)
                {
                    array[index] = (UInt16)node.Value;
                    node = node.Next;
                    index++;
                }
            }
        }
    }

    static void CountingSort(UInt16[] array)
    {
        var size = array.Length;
        var max = 0;
        for (int i = 1; i < size; i++)
        {
            if (array[i] > max)
            {
                max = array[i];
            }
        }

        var entries = new int[max + 1];

        for (int i = 0; i < max + 1; i++)
        {
            entries[i] = 0;
        }

        for (int i = 0; i < size; i++)
        {
            entries[array[i]]++;
        }

        for (UInt16 i = 0, j = 0; i <= max; i++)
        {
            while (entries[i] > 0)
            {
                array[j] = i;
                j++;
                entries[i]--;
            }
        }
    }


    static void RadixSort(UInt16[] array)
    {
        RadixSort(array, array.Length);
    }


    static void RadixSort(UInt16[] array, int size)
    {
        var max = GetMax(array, size);

        for (int exponent = 1; max / exponent > 0; exponent *= 10)
        {
            CountingSort(array, size, exponent);
        }
    }

    public static UInt16 GetMax(UInt16[] array, int size)
    {
        var max = array[0];

        for (int i = 1; i < size; i++)
        {
            if (array[i] > max)
            {
                max = array[i];
            }
        }


        return max;
    }

    static void CountingSort(UInt16[] array, int size, int exponent)
    {
        var outputArr = new UInt16[size];
        var entries = new int[10];

        for (int i = 0; i < 10; i++)
            entries[i] = 0;

        for (int i = 0; i < size; i++)
            entries[(array[i] / exponent) % 10]++;

        for (int i = 1; i < 10; i++)
            entries[i] += entries[i - 1];

        for (int i = size - 1; i >= 0; i--)
        {
            outputArr[entries[(array[i] / exponent) % 10] - 1] = array[i];
            entries[(array[i] / exponent) % 10]--;
        }

        for (int i = 0; i < size; i++)
            array[i] = outputArr[i];
    }
}
